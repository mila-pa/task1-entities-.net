import java.util.ArrayList;

/**
 * Created by panteyenko on 06/08/2017.
 */
public class EntityMain {
    public static void main(String[] args) {
        int count = 0;
        ArrayList<AliveEntity> aliveEntities = new ArrayList<AliveEntity>();
        aliveEntities.add(new Fish(11, "Plotva"));
        aliveEntities.add(new Fish(12, "Plotva"));
        aliveEntities.add(new Animal(13, "Plotva", 33));
        aliveEntities.add(new Animal(14, "Plotva", 765));
//        AliveEntity[] aliveEntities = new AliveEntity[4];
//        aliveEntities[0] = new Fish(11, "Plotva");
//        aliveEntities[1] = new Fish(12, "Karas");
//        aliveEntities[2] = new Animal(13, "Horse", 6);
//        aliveEntities[3] = new Animal(14, "Dog", 4);

//        for (AliveEntity a : aliveEntities) {
//           System.out.println(a.name);
//        }
        Eat e = new Karas(7, "karasik");
        //    System.out.println(e.canEat(true));
        e.canEat("Конечно же нет");

        for (AliveEntity a : aliveEntities) {
            //   System.out.println(a.id);
            if (a instanceof Animal) {
                Animal animals = (Animal) a;
                count = count + animals.getLegs();
            }
        }
        System.out.println("Количество ног = " + count);

        for (AliveEntity b : aliveEntities) {
            if (b instanceof Breathe) {
                Breathe breathe = (Breathe) b;
                System.out.println("Entity ID who can breath under the water " + b.id);
            }
        }

    } //psvm
} //main
