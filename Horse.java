/**
 * Created by panteyenko on 06/09/2017.
 */
public class Horse extends Animal implements Eat {
    public Horse(int id, String name, int legs) {
        super(id, name, legs);
    }

    @Override
    public void canEat(String horseFood) {

    }
}
