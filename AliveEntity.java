/**
 * Created by panteyenko on 06/08/2017.
 */
public class AliveEntity extends Entity {
    String name;

    public AliveEntity(int id, String name) {
        super(id);
        this.name = name;
    }
}
