/**
 * Created by panteyenko on 06/08/2017.
 */
public class Animal extends AliveEntity {

    int legs;

    public Animal(int id, String name, int legs) {
        super(id, name);
        this.legs = legs;
    }

    public int getLegs() {
        return legs;
    }
}
