/**
 * Created by panteyenko on 06/08/2017.
 */
public class Fish extends AliveEntity implements Breathe{

    public Fish(int id, String name) {
        super(id, name);
    }

    @Override
    public boolean canBreathe() {
        return false;
    }
}
