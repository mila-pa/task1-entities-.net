/**
 * Created by panteyenko on 06/09/2017.
 */
public class Dog extends Animal implements Breathe {
    public Dog(int id, String name, int legs) {
        super(id, name, legs);
    }

    @Override
    public boolean canBreathe() {
        return false;
    }
}
