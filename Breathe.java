/**
 * Created by panteyenko on 06/08/2017.
 */
public interface Breathe {

    boolean canBreathe();

}
